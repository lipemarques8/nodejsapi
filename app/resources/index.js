import Login from './login';
import Register from './register';
import Users from './users';

import mongoose from 'mongoose';

const User = mongoose.model('User');

export const loginResource = new Login(User);
export const registerResource = new Register(User);
export const usersResource = new Users(User);
