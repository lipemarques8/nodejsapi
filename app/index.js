import mongoose from 'mongoose';
import express from 'express';
import bodyParser from 'body-parser';

import './models';

import config from './config';
import routes from './routes';
import cors from './lib/cors';
import errors from './lib/errors';

const app = express();
export default app;

mongoose.connect(config.database);

app.use(cors);
app.use(bodyParser());
app.use(routes);
app.use(errors);

app.listen(process.env.PORT || 3000);
